# Maintainer: Mark Wagie <mark at manjaro dot org>
# Maintainer: Philip Müller <philm@manjaro.org>
# Contributor: Guillaume Benoit <guillaume@manjaro.org>
# Contributor: Helmut Stult

pkgbase=pamac
pkgname=('pamac-gtk' 'pamac-gnome-integration')
pkgver=11.7.2
pkgrel=2
pkgdesc="A Package Manager based on libalpm with AUR and Appstream support (GTK4)"
arch=('x86_64' 'aarch64')
url="https://github.com/manjaro/pamac"
license=('GPL-3.0-or-later')
depends=(
  'archlinux-appstream-data'
  'gtk3'
  'gtk4'
  'libadwaita'
  'libnotify'
  'libpamac>=11.7.0'
  'pamac-cli>=11.7.0'
)
makedepends=(
  'asciidoc'
  'git'
  'gobject-introspection'
  'meson'
  'vala'
)
options=('debug')
source=("git+https://github.com/manjaro/pamac.git#tag=$pkgver")
sha256sums=('b7166976642b21e9ce22c9f0a0b6a2708ffe4f1c91cf9ec3b52231d2eb13d66e')

prepare() {
  cd "$pkgbase"
  local src
  for src in "${source[@]}"; do
      src="${src%%::*}"
      src="${src##*/}"
      [[ $src = *.patch ]] || continue
      echo "Applying patch $src..."
      patch -Np1 < "../$src"
  done

  # adjust version string
  sed -i -e "s|\"$pkgver\"|\"$pkgver-$pkgrel\"|g" src/version.vala
}

build() {
  arch-meson "$pkgbase" build \
    -Denable-fake-gnome-software=true
  meson compile -C build
}

check() {
  cd "$pkgbase"
  desktop-file-validate data/applications/*.desktop || :
}

package_pamac-gtk() {
  optdepends=(
    'pamac-gnome-integration: for integration into GNOME'
    'libpamac-flatpak-plugin: for Flatpak support'
    'libpamac-snap-plugin: for Snap support'
#  'libpamac-aur-plugin: for AUR support'
#  'libpamac-appstream-plugin: for Appstream support'
  )
  provides=(
    'pamac'
    'libpamac-gtk.so'
  )
  conflicts=(
    'pamac-gtk3'
  )
  install="$pkgname.install"

  meson install -C build --destdir "$pkgdir"

  cd "$pkgbase"

  # remove pamac-gnome-integration
  rm "$pkgdir/usr/bin/gnome-software"
  rm "$pkgdir/usr/share/applications/org.gnome.Software.desktop"
  rm "$pkgdir/usr/share/dbus-1/services/org.gnome.Software.service"
}

package_pamac-gnome-integration() {
  pkgdesc="Pamac GNOME integration"
  depends=('pamac-gtk')
  conflicts=('gnome-software')

  install -Dm644 "build/data/dbus/org.gnome.Software.service" \
    "$pkgdir/usr/share/dbus-1/services/org.gnome.Software.service"
  install -Dm755 "build/src/gnome-software" "$pkgdir/usr/bin/gnome-software"

  cd "$srcdir/$pkgbase"
  install -Dm644 "data/applications/org.gnome.Software.desktop" \
    "$pkgdir/usr/share/applications/org.gnome.Software.desktop"
}
